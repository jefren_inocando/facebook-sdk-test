<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'likes-detail-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'page_id'); ?>
		<?php echo $form->textArea($model,'page_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'page_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_id'); ?>
		<?php echo $form->textArea($model,'post_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'post_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'individual_name'); ?>
		<?php echo $form->textArea($model,'individual_name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'individual_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'individual_category'); ?>
		<?php echo $form->textArea($model,'individual_category',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'individual_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'individual_id'); ?>
		<?php echo $form->textArea($model,'individual_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'individual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_aquired_time'); ?>
		<?php echo $form->textArea($model,'data_aquired_time',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'data_aquired_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->