<?php
$this->breadcrumbs=array(
	'Likes Details',
);

$this->menu=array(
	array('label'=>'Create LikesDetail', 'url'=>array('create')),
	array('label'=>'Manage LikesDetail', 'url'=>array('admin')),
);
?>

<h1>Likes Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
