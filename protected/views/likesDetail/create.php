<?php
$this->breadcrumbs=array(
	'Likes Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LikesDetail', 'url'=>array('index')),
	array('label'=>'Manage LikesDetail', 'url'=>array('admin')),
);
?>

<h1>Create LikesDetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>