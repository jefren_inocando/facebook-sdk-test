<?php
$this->breadcrumbs=array(
	'Likes Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LikesDetail', 'url'=>array('index')),
	array('label'=>'Create LikesDetail', 'url'=>array('create')),
	array('label'=>'View LikesDetail', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage LikesDetail', 'url'=>array('admin')),
);
?>

<h1>Update LikesDetail <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>