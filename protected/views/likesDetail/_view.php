<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_id')); ?>:</b>
	<?php echo CHtml::encode($data->page_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_id')); ?>:</b>
	<?php echo CHtml::encode($data->post_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('individual_name')); ?>:</b>
	<?php echo CHtml::encode($data->individual_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('individual_category')); ?>:</b>
	<?php echo CHtml::encode($data->individual_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('individual_id')); ?>:</b>
	<?php echo CHtml::encode($data->individual_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_aquired_time')); ?>:</b>
	<?php echo CHtml::encode($data->data_aquired_time); ?>
	<br />


</div>