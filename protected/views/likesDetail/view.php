<?php
$this->breadcrumbs=array(
	'Likes Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LikesDetail', 'url'=>array('index')),
	array('label'=>'Create LikesDetail', 'url'=>array('create')),
	array('label'=>'Update LikesDetail', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LikesDetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LikesDetail', 'url'=>array('admin')),
);
?>

<h1>View LikesDetail #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'page_id',
		'post_id',
		'individual_name',
		'individual_category',
		'individual_id',
		'data_aquired_time',
	),
)); ?>
