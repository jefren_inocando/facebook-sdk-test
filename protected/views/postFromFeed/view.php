<?php
$this->breadcrumbs=array(
	'Post From Feeds'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PostFromFeed', 'url'=>array('index')),
	array('label'=>'Create PostFromFeed', 'url'=>array('create')),
	array('label'=>'Update PostFromFeed', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PostFromFeed', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PostFromFeed', 'url'=>array('admin')),
);
?>

<h1>View PostFromFeed #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'page_id',
		'post_id',
		'from_name',
		'from_category',
		'from_id',
		'page_owner',
		'to_name',
		'to_category',
		'to_id',
		'message',
		'message_tags',
		'picture',
		'link',
		'name',
		'caption',
		'description',
		'source',
		'properties',
		'action_link_comment',
		'likes',
		'action_name_comment',
		'action_link_like',
		'privacy_description',
		'privacy_value',
		'icon',
		'type',
		'action_name_like',
		'story',
		'story_tags',
		'object_id',
		'with_tags',
		'comments',
		'application_name',
		'created_time',
		'application_id',
		'data_aquired_time',
		'place',
		'updated_time',
	),
)); ?>
