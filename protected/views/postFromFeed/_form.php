<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-from-feed-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'page_id'); ?>
		<?php echo $form->textArea($model,'page_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'page_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_id'); ?>
		<?php echo $form->textArea($model,'post_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'post_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_name'); ?>
		<?php echo $form->textArea($model,'from_name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'from_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_category'); ?>
		<?php echo $form->textArea($model,'from_category',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'from_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_id'); ?>
		<?php echo $form->textArea($model,'from_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'from_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'page_owner'); ?>
		<?php echo $form->textArea($model,'page_owner',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'page_owner'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_name'); ?>
		<?php echo $form->textArea($model,'to_name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'to_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_category'); ?>
		<?php echo $form->textArea($model,'to_category',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'to_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_id'); ?>
		<?php echo $form->textArea($model,'to_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'to_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message_tags'); ?>
		<?php echo $form->textArea($model,'message_tags',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message_tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'picture'); ?>
		<?php echo $form->textArea($model,'picture',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'picture'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'link'); ?>
		<?php echo $form->textArea($model,'link',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'link'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textArea($model,'name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'caption'); ?>
		<?php echo $form->textArea($model,'caption',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'caption'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'source'); ?>
		<?php echo $form->textArea($model,'source',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'source'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'properties'); ?>
		<?php echo $form->textArea($model,'properties',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'properties'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action_link_comment'); ?>
		<?php echo $form->textArea($model,'action_link_comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'action_link_comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'likes'); ?>
		<?php echo $form->textArea($model,'likes',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'likes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action_name_comment'); ?>
		<?php echo $form->textArea($model,'action_name_comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'action_name_comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action_link_like'); ?>
		<?php echo $form->textArea($model,'action_link_like',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'action_link_like'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy_description'); ?>
		<?php echo $form->textArea($model,'privacy_description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'privacy_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy_value'); ?>
		<?php echo $form->textArea($model,'privacy_value',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'privacy_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'icon'); ?>
		<?php echo $form->textArea($model,'icon',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'icon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textArea($model,'type',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action_name_like'); ?>
		<?php echo $form->textArea($model,'action_name_like',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'action_name_like'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'story'); ?>
		<?php echo $form->textArea($model,'story',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'story'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'story_tags'); ?>
		<?php echo $form->textArea($model,'story_tags',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'story_tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'object_id'); ?>
		<?php echo $form->textArea($model,'object_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'object_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'with_tags'); ?>
		<?php echo $form->textArea($model,'with_tags',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'with_tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comments'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'application_name'); ?>
		<?php echo $form->textArea($model,'application_name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'application_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textArea($model,'created_time',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'application_id'); ?>
		<?php echo $form->textArea($model,'application_id',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'application_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_aquired_time'); ?>
		<?php echo $form->textArea($model,'data_aquired_time',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'data_aquired_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'place'); ?>
		<?php echo $form->textArea($model,'place',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textArea($model,'updated_time',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->