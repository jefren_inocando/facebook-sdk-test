<?php
$this->breadcrumbs=array(
	'Post From Feeds'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PostFromFeed', 'url'=>array('index')),
	array('label'=>'Create PostFromFeed', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('post-from-feed-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Post From Feeds</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'post-from-feed-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'page_id',
		'post_id',
		'from_name',
		'from_category',
		'from_id',
		/*
		'page_owner',
		'to_name',
		'to_category',
		'to_id',
		'message',
		'message_tags',
		'picture',
		'link',
		'name',
		'caption',
		'description',
		'source',
		'properties',
		'action_link_comment',
		'likes',
		'action_name_comment',
		'action_link_like',
		'privacy_description',
		'privacy_value',
		'icon',
		'type',
		'action_name_like',
		'story',
		'story_tags',
		'object_id',
		'with_tags',
		'comments',
		'application_name',
		'created_time',
		'application_id',
		'data_aquired_time',
		'place',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
