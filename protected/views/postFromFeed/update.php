<?php
$this->breadcrumbs=array(
	'Post From Feeds'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PostFromFeed', 'url'=>array('index')),
	array('label'=>'Create PostFromFeed', 'url'=>array('create')),
	array('label'=>'View PostFromFeed', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PostFromFeed', 'url'=>array('admin')),
);
?>

<h1>Update PostFromFeed <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>