<?php
$this->breadcrumbs=array(
	'Post From Feeds',
);

$this->menu=array(
	array('label'=>'Create PostFromFeed', 'url'=>array('create')),
	array('label'=>'Manage PostFromFeed', 'url'=>array('admin')),
);
?>

<h1>Post From Feeds</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
