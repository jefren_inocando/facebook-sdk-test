<?php
$this->breadcrumbs=array(
	'Post From Feeds'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PostFromFeed', 'url'=>array('index')),
	array('label'=>'Manage PostFromFeed', 'url'=>array('admin')),
);
?>

<h1>Create PostFromFeed</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>