<h1>Download CSV files:</h1>

<!-- success flash message -->
<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php echo CHtml::link('Dowload post data for ' . $page_id, array('site/postcsv', 'page_id' => $page_id)) ?> <br><br>

<?php echo CHtml::link('Dowload Like data of each post for ' . $page_id, array('site/likecsv', 'page_id' => $page_id)) ?>