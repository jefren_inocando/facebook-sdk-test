<?php

/**
 * LinkForm class.
 */
class LinkForm extends CFormModel {

    public $path;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('path', 'required'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'path' => 'Facebook page or ID',
        );
    }

}