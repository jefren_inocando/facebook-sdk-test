<?php

/**
 * This is the model class for table "post_from_feed".
 *
 * The followings are the available columns in table 'post_from_feed':
 * @property integer $id
 * @property string $page_id
 * @property string $post_id
 * @property string $from_name
 * @property string $from_category
 * @property string $from_id
 * @property string $page_owner
 * @property string $to_name
 * @property string $to_category
 * @property string $to_id
 * @property string $message
 * @property string $message_tags
 * @property string $picture
 * @property string $link
 * @property string $name
 * @property string $caption
 * @property string $description
 * @property string $source
 * @property string $properties
 * @property string $action_link_comment
 * @property string $likes
 * @property string $action_name_comment
 * @property string $action_link_like
 * @property string $privacy_description
 * @property string $privacy_value
 * @property string $icon
 * @property string $type
 * @property string $action_name_like
 * @property string $story
 * @property string $story_tags
 * @property string $object_id
 * @property string $with_tags
 * @property string $comments
 * @property string $application_name
 * @property string $created_time
 * @property string $application_id
 * @property string $data_aquired_time
 * @property string $place
 * @property string $updated_time
 */
class PostFromFeed extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PostFromFeed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post_from_feed';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id, post_id, from_name, from_category, from_id, page_owner, to_name, to_category, to_id, message, message_tags, picture, link, name, caption, description, source, properties, action_link_comment, likes, action_name_comment, action_link_like, privacy_description, privacy_value, icon, type, action_name_like, story, story_tags, object_id, with_tags, comments, application_name, created_time, application_id, data_aquired_time, place, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, page_id, post_id, from_name, from_category, from_id, page_owner, to_name, to_category, to_id, message, message_tags, picture, link, name, caption, description, source, properties, action_link_comment, likes, action_name_comment, action_link_like, privacy_description, privacy_value, icon, type, action_name_like, story, story_tags, object_id, with_tags, comments, application_name, created_time, application_id, data_aquired_time, place, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'post_id' => 'Post',
			'from_name' => 'From Name',
			'from_category' => 'From Category',
			'from_id' => 'From',
			'page_owner' => 'Page Owner',
			'to_name' => 'To Name',
			'to_category' => 'To Category',
			'to_id' => 'To',
			'message' => 'Message',
			'message_tags' => 'Message Tags',
			'picture' => 'Picture',
			'link' => 'Link',
			'name' => 'Name',
			'caption' => 'Caption',
			'description' => 'Description',
			'source' => 'Source',
			'properties' => 'Properties',
			'action_link_comment' => 'Action Link Comment',
			'likes' => 'Likes',
			'action_name_comment' => 'Action Name Comment',
			'action_link_like' => 'Action Link Like',
			'privacy_description' => 'Privacy Description',
			'privacy_value' => 'Privacy Value',
			'icon' => 'Icon',
			'type' => 'Type',
			'action_name_like' => 'Action Name Like',
			'story' => 'Story',
			'story_tags' => 'Story Tags',
			'object_id' => 'Object',
			'with_tags' => 'With Tags',
			'comments' => 'Comments',
			'application_name' => 'Application Name',
			'created_time' => 'Created Time',
			'application_id' => 'Application',
			'data_aquired_time' => 'Data Aquired Time',
			'place' => 'Place',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id,true);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('from_name',$this->from_name,true);
		$criteria->compare('from_category',$this->from_category,true);
		$criteria->compare('from_id',$this->from_id,true);
		$criteria->compare('page_owner',$this->page_owner,true);
		$criteria->compare('to_name',$this->to_name,true);
		$criteria->compare('to_category',$this->to_category,true);
		$criteria->compare('to_id',$this->to_id,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('message_tags',$this->message_tags,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('caption',$this->caption,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('properties',$this->properties,true);
		$criteria->compare('action_link_comment',$this->action_link_comment,true);
		$criteria->compare('likes',$this->likes,true);
		$criteria->compare('action_name_comment',$this->action_name_comment,true);
		$criteria->compare('action_link_like',$this->action_link_like,true);
		$criteria->compare('privacy_description',$this->privacy_description,true);
		$criteria->compare('privacy_value',$this->privacy_value,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('action_name_like',$this->action_name_like,true);
		$criteria->compare('story',$this->story,true);
		$criteria->compare('story_tags',$this->story_tags,true);
		$criteria->compare('object_id',$this->object_id,true);
		$criteria->compare('with_tags',$this->with_tags,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('application_name',$this->application_name,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('application_id',$this->application_id,true);
		$criteria->compare('data_aquired_time',$this->data_aquired_time,true);
		$criteria->compare('place',$this->place,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchCsv($page_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$page_id);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('from_name',$this->from_name,true);
		$criteria->compare('from_category',$this->from_category,true);
		$criteria->compare('from_id',$this->from_id,true);
		$criteria->compare('page_owner',$this->page_owner,true);
		$criteria->compare('to_name',$this->to_name,true);
		$criteria->compare('to_category',$this->to_category,true);
		$criteria->compare('to_id',$this->to_id,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('message_tags',$this->message_tags,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('caption',$this->caption,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('properties',$this->properties,true);
		$criteria->compare('action_link_comment',$this->action_link_comment,true);
		$criteria->compare('likes',$this->likes,true);
		$criteria->compare('action_name_comment',$this->action_name_comment,true);
		$criteria->compare('action_link_like',$this->action_link_like,true);
		$criteria->compare('privacy_description',$this->privacy_description,true);
		$criteria->compare('privacy_value',$this->privacy_value,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('action_name_like',$this->action_name_like,true);
		$criteria->compare('story',$this->story,true);
		$criteria->compare('story_tags',$this->story_tags,true);
		$criteria->compare('object_id',$this->object_id,true);
		$criteria->compare('with_tags',$this->with_tags,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('application_name',$this->application_name,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('application_id',$this->application_id,true);
		$criteria->compare('data_aquired_time',$this->data_aquired_time,true);
		$criteria->compare('place',$this->place,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}