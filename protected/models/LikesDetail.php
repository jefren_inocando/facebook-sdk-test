<?php

/**
 * This is the model class for table "likes_detail_in_post".
 *
 * The followings are the available columns in table 'likes_detail_in_post':
 * @property integer $id
 * @property string $page_id
 * @property string $post_id
 * @property string $individual_name
 * @property string $individual_category
 * @property string $individual_id
 * @property string $data_aquired_time
 */
class LikesDetail extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LikesDetail the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'likes_detail_in_post';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('page_id, post_id, individual_name, individual_category, individual_id, data_aquired_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, page_id, post_id, individual_name, individual_category, individual_id, data_aquired_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'page_id' => 'Page',
            'post_id' => 'Post',
            'individual_name' => 'Individual Name',
            'individual_category' => 'Individual Category',
            'individual_id' => 'Individual',
            'data_aquired_time' => 'Data Aquired Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('page_id', $this->page_id, true);
        $criteria->compare('post_id', $this->post_id, true);
        $criteria->compare('individual_name', $this->individual_name, true);
        $criteria->compare('individual_category', $this->individual_category, true);
        $criteria->compare('individual_id', $this->individual_id, true);
        $criteria->compare('data_aquired_time', $this->data_aquired_time, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchCsv($page_id) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('page_id', $page_id);
        $criteria->compare('post_id', $this->post_id, true);
        $criteria->compare('individual_name', $this->individual_name, true);
        $criteria->compare('individual_category', $this->individual_category, true);
        $criteria->compare('individual_id', $this->individual_id, true);
        $criteria->compare('data_aquired_time', $this->data_aquired_time, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}