<?php

class SiteController extends Controller {

    /**
     * FB query page
     */
    public function actionFacebook() {
        $model = new LinkForm;

        if (isset($_POST['LinkForm'])) {
            ini_set('max_execution_time', 999); // for long loops
            $model->attributes = $_POST['LinkForm'];
            if ($model->validate()) {

                $facebook = new Facebook(array(// initialize SDK using credentials from login page
                            'appId' => Yii::app()->user->appid,
                            'secret' => Yii::app()->user->secret,
                        ));

                $data = $facebook->api(trim($model->path)); // query page on API

                $page_id = !empty($data['id']) ? (string) $data['id'] : null;
                if (!empty($page_id)) {

                    $posts = $facebook->api($page_id . '/feed'); // query post data
                    if (!empty($posts['data'])) {
                        $this->AddPosts($facebook, $posts['data'], $page_id); // insert post data

                        Yii::app()->user->setFlash('success', '<h3>Submitted successfully</h3> Check at posts and likes link on the menu. I used SQLite for my DB');

                        $this->redirect(array('site/export', 'page_id' => $page_id)); // redirect to CSV download page
                    }
                }
            }
        }

        $this->render('index', array('model' => $model));
    }

    /**
     * Export to CSV
     */
    public function actionExport($page_id) {
        $this->render('export', array('page_id' => $page_id));
    }

    /**
     * Download Post data using CSV used EExcelview extension
     * @param type $page_id 
     */
    public function actionPostCsv($page_id) {
        $post = new PostFromFeed('search');

        $this->widget('EExcelView', array(
            'dataProvider' => $post->searchCsv($page_id),
            'title' => 'Post_from_feed_' . $page_id,
            'filename' => 'post_from_feed_' . $page_id,
            'grid_mode' => 'export',
            'exportType' => 'CSV',
            'columns' => array(
                'page_id',
                'post_id',
                'from_name',
                'from_category',
                'from_id',
                'page_owner',
                'to_name',
                'to_category',
                'to_id',
                'message',
                'message_tags',
                'picture',
                'link',
                'name',
                'caption',
                'description',
                'source',
                'properties',
                'action_link_comment',
                'likes',
                'action_name_comment',
                'action_link_like',
                'privacy_description',
                'privacy_value',
                'icon',
                'type',
                'action_name_like',
                'story',
                'story_tags',
                'object_id',
                'with_tags',
                'comments',
                'application_name',
                'created_time',
                'application_id',
                'data_aquired_time',
                'place',
                'updated_time',
            )
        ));
    }

    /**
     * Download like CSV data used EEXcelview extension
     * @param type $page_id 
     */
    public function actionLikeCsv($page_id) {
        $likes = new LikesDetail('search');

        $this->widget('EExcelView', array(
            'dataProvider' => $likes->searchCsv($page_id),
            'title' => 'likes_detail_in_post_' . $page_id,
            'filename' => 'likes_detail_in_post_' . $page_id,
            'grid_mode' => 'export',
            'exportType' => 'CSV',
            'stream' => TRUE,
            'columns' => array(
                'page_id',
                'post_id',
                'individual_name',
                'individual_category',
                'individual_id',
                'data_aquired_time',
            )
        ));
    }

    /**
     * Insert Post data of page based facebook page id or name
     * @param type $facebook
     * @param type $data
     * @param type $page_id 
     */
    public function AddPosts($facebook, $data, $page_id) {

        foreach ($data as $value) {

            $model = new PostFromFeed();
            $model->page_id = $page_id;
            $model->post_id = !empty($value['id']) ? $value['id'] : null;
            $model->from_name = !empty($value['from']['name']) ? $value['from']['name'] : null;
            $model->from_category = !empty($value['from']['category']) ? $value['from']['category'] : null;
            $model->from_id = !empty($value['from']['id']) ? $value['from']['id'] : null;
            $model->page_owner = !empty($value['from']['id']) ? ($page_id == $value['from']['id'] ? 1 : 0) : null;
            $model->to_name = !empty($value['to']['data']['0']['name']) ? $value['to']['data']['0']['name'] : null;
            $model->to_category = !empty($value['to']['data']['0']['category']) ? $value['to']['data']['0']['category'] : null;
            $model->to_id = !empty($value['to']['data']['0']['id']) ? $value['to']['data']['0']['id'] : null;
            $model->message = !empty($value['message']) ? $value['message'] : null;
            $model->message_tags = !empty($value['message_tags']) ? 1 : 0;
            $model->picture = !empty($value['picture']) ? $value['picture'] : null;
            $model->link = !empty($value['link']) ? $value['link'] : null;
            $model->name = !empty($value['name']) ? $value['name'] : null;
            $model->caption = !empty($value['caption']) ? $value['caption'] : null;
            $model->description = !empty($value['description']) ? $value['description'] : null;
            $model->source = !empty($value['source']) ? $value['source'] : null;
            $model->properties = !empty($value['properties']) ? 1 : 0; // I made it like message_tags because the value is array
            $model->icon = !empty($value['icon']) ? $value['icon'] : null;

            //@TODO action name comment etc cannot find it

            $model->privacy_value = !empty($value['privacy']['value']) ? $value['privacy']['value'] : null;
            $model->privacy_description = !empty($value['privacy']['description']) ? $value['privacy']['description'] : null;
            $model->type = !empty($value['type']) ? $value['type'] : null;

            if (!empty($value['likes']['data'])) {
                $likes = $facebook->api($value['id'] . '/likes?summary=true');
                $model->likes = !empty($likes['summary']['total_count']) ? $likes['summary']['total_count'] : 0; // get total likes from summary field

                $this->AddLikes($value['likes']['data'], $page_id, $value['id']); // add likes of post
            } else {
                $model->likes = 0;
            }

            $model->place = !empty($value['place']) ? $value['place'] : null;
            $model->story = !empty($value['story']) ? $value['story'] : null;
            $model->story_tags = !empty($value['story_tags']) ? 1 : 0;
            $model->with_tags = !empty($value['with_tags']) ? 1 : 0;

            if (!empty($value['comments']['data'])) {
                $comments = $facebook->api($value['id'] . '/comments?summary=true');
                $model->comments = !empty($comments['summary']['total_count']) ? $comments['summary']['total_count'] : 0; // get total likes from summary field
            } else {
                $model->comments = 0;
            }

            $model->object_id = !empty($value['object_id']) ? $value['object_id'] : null;
            $model->application_name = !empty($value['application']['name']) ? $value['application']['name'] : null;
            $model->application_id = !empty($value['application']['id']) ? $value['application']['id'] : null;
            $model->created_time = !empty($value['created_time']) ? $value['created_time'] : null;
            $model->updated_time = !empty($value['updated_time']) ? $value['updated_time'] : null;
            $model->data_aquired_time = date('Y-m-d h:i:s');
            $model->save();
        }
    }

    /**
     * Inserts the likes data of each post (did not insert all of likes too many)
     * @param type $data array of likes of post
     * @param type $page_id id of page
     * @param type $post_id id of post
     */
    public function AddLikes($data, $page_id, $post_id) {

        foreach ($data as $value) {
            $model = new LikesDetail();
            $model->page_id = $page_id;
            $model->post_id = $post_id;
            $model->individual_name = !empty($value['name']) ? $value['name'] : null;
            $model->individual_id = !empty($value['id']) ? $value['id'] : null;
            $model->data_aquired_time = date('Y-m-d h:i:s');
            $model->save();
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionIndex() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login())
                $this->redirect(array('site/facebook'));
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}